Link to wiki:

[Wiki](https://git.rwth-aachen.de/pascal.roth.18/ba-pascal-roth/-/wikis/home)

| ID  | creator     | code               | location                   | decription                      | start date | end date |
|-----|-------------|--------------------|----------------------------|---------------------------------|------------|----------|
| 000 | Pascal Roth | Cantera Python API | local                      | homogenous reaction simulations | 07.04.20   | 27.08.20 |
| 001 | Pascal Roth | pyTorch            | local                      | machine learning with pyTorch   | 14.04.20   | 27.08.20 |
| 002 | Pascal Roth |                    | local                      | model comparison                | 30.07.20   | 27.08.20 |
| 003 | Pascal Roth |                    | local                      | Conda environment               | 07.04.20   | 27.08.20 |

The used conda environment was to run all the codes was exported into "003-conda_environment/environment.yml".
To replicate this exact environment run 
```bash
conda env create -f /path/to/file.yml
```

| Software | Version | Source              | commit |
|----------|---------|---------------------|--------|
| pyTorch  | 1.4     | installed via conda |        |
| Cantera  | 2.2     | installed via conda |        |
